import networkx as nx
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import community

def removeNode(web,node):
	tempWeb = nx.DiGraph()
	tempWeb.add_nodes_from(web.nodes())
	tempWeb.add_edges_from(web.edges())
	tempWeb.remove_nodes_from([node])
	return tempWeb

def getModularity(web):
	D = web.to_undirected()
	dendo = community.generate_dendogram(D, None)
	partition = community.partition_at_level(dendo,len(dendo)-1)
	return community.modularity(partition, D)

with open('edgelist.csv', 'rb') as inf:
    next(inf, '')   # skip a line
    G = nx.read_edgelist(inf, delimiter=',', nodetype=str, data=(('weight',float),), encoding="utf-8")

orig_mod = getModularity(G)
delta_mod = [orig_mod - getModularity(removeNode(G,n)) for n in G]

plt.plot(np.sort(delta_mod),'ok')
plt.show()

plt.axis('off')
pos = nx.spring_layout(G,iterations=100)
nx.draw_networkx_edges(G,pos,alpha=0.07)
nodes = nx.draw_networkx_nodes(G,pos,node_size=20,node_color=delta_mod,cmap=plt.cm.Spectral,linewidths=0.0,weight=None)

plt.sci(nodes)
plt.colorbar()
plt.show()
