__author__ = 'Debraj Roy'
import csv
import networkx as nx
import numpy
import community
import matplotlib.pyplot as plt

def avg_neigh_degree(g):
     data = {}
     for n in g.nodes():
         if g.degree(n):
             data[n] = float(sum(g.degree(i) for i in g[n]))/g.degree(n)
     return data

def highest_centrality(cent_dict):
     """Returns a tuple (node,value) with the node with largest value from Networkx centrality dictionary."""
     # Create ordered tuple of centrality data
     cent_items=[(b,a) for (a,b) in cent_dict.iteritems()]
     # Sort in descending order
     cent_items.sort()
     cent_items.reverse()
     return tuple(reversed(cent_items[0]))

def centrality_scatter(dict1,dict2,path="",ylab="",xlab="",title="",line=False):
     # Create figure and drawing axis
     fig = plt.figure(figsize=(7,7))
     ax1 = fig.add_subplot(111)
     # Create items and extract centralities
     items1 = sorted(dict1.items())
     items2 = sorted(dict2.items())
     xdata=[b for a,b in items1]
     ydata=[b for a,b in items2]

     # Add each actor to the plot by ID
     for p in xrange(len(items1)):
         ax1.text(x=xdata[p], y=ydata[p],s=str(items1[p][0]), color="b")
     if line:
     # use NumPy to calculate the best fit
         slope, yint = plt.polyfit(xdata,ydata,1)
         xline = plt.xticks()[0]
         yline = map(lambda x: slope*x+yint,xline)
         ax1.plot(xline,yline,ls='--',color='b')
         # Set new x- and y-axis limits


     plt.xlim((0.0,max(xdata)+(.15*max(xdata))))
     plt.ylim((0.0,max(ydata)+(.15*max(ydata))))
     # Add labels and save
     ax1.set_title(title)
     ax1.set_xlabel(xlab)
     ax1.set_ylabel(ylab)
     plt.savefig(path)


with open('edgelist.csv', 'rb') as inf:
    next(inf, '')   # skip a line
    G = nx.read_edgelist(inf, delimiter=',',create_using=nx.DiGraph(), nodetype=str, data=(('weight',float),), encoding="utf-8")

N,K = G.order(), G.size()
avg_deg = float(K)/N
print "Nodes: ", N
print "Edges: ", K
print "Average degree: ", avg_deg
#print G.number_of_nodes()

in_degrees = G.in_degree() # dictionary node:degree
in_values = sorted(set(in_degrees.values()))
in_hist = [in_degrees.values().count(x) for x in in_values]

out_degrees = G.out_degree() # dictionary node:degree
out_values = sorted(set(out_degrees.values()))
out_hist = [out_degrees.values().count(x) for x in out_values]


plt.figure()
plt.plot(in_values,in_hist,'ro-') # in-degree
plt.plot(out_values,out_hist,'bv-') # out-degree
plt.legend(['In-degree','Out-degree'])
plt.xlabel('Degree')
plt.ylabel('Number of nodes')
plt.title('Airport network')
plt.savefig('Airport_Network_Degree_Distribution.pdf')
plt.close()

G_ud = G.to_undirected()
# Clustering coefficient of all nodes (in a dictionary)
clust_coefficients = nx.clustering(G_ud)
# Average clustering coefficient
ccs = nx.clustering(G_ud)
avg_clust = sum(ccs.values()) / len(ccs)

G_components = nx.connected_component_subgraphs(G_ud)
G_mc = next(G_components, None)
#G_mc = row2[0]
# Betweenness centrality
bet_cen = nx.betweenness_centrality(G_mc)
# Closeness centrality
clo_cen = nx.closeness_centrality(G_mc)
# Eigenvector centrality
eig_cen = nx.eigenvector_centrality(G_mc)

centrality_scatter(bet_cen,clo_cen,path = "BC",xlab="Betweenness",ylab="Eigenvector",title="Airport Network",line=False)
results = [(k,bet_cen[k],clo_cen[k],eig_cen[k]) for k in G_mc]
f = open('Airport_network_results.txt','w')
for item in results:
     f.write(','.join(map(str,item)))
     f.write('\n')
f.close()

nx.transitivity(G_ud)
#find modularity
part = community.best_partition(G_ud)
mod = community.modularity(part,G_ud)

#plot, color nodes using community structure
values = [part.get(node) for node in G_ud.nodes()]
nx.draw_spring(G_ud, cmap = plt.get_cmap('jet'), node_color = values, node_size=30, with_labels=False)
plt.show()


